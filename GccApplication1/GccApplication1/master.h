/*
 * master.h
 *
 * Created: 11/15/2014 12:10:40 AM
 *  Author: Chris
 */ 
#include <avr/io.h>

extern const uint8_t RAM_ADDR;
extern const uint8_t DEFAULT_ATMEGA16_ADDR;
extern const uint8_t LENGTH;

void master_role(void);