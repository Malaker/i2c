/*
 * slave.h
 *
 * Created: 11/14/2014 11:44:10 PM
 *  Author: Chris
 */ 
#include <avr/io.h>

extern const uint8_t DEFAULT_ATMEGA16_ADDR;
extern const uint8_t LENGTH;
extern const uint8_t RAM_ADDR;

void init_slave(void);
void slave_role();

